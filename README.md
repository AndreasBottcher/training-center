Центр тестирования/обучения персонала
=====================================

Приложение, предназначенное для создания (администратором) и прохождения (персоналом) тестов.

Базируется на Symfony 3.2.4, проект начат 19.02.2017 года.
