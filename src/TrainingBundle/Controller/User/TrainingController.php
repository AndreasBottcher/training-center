<?php

namespace TrainingBundle\Controller\User;

use TrainingBundle\Entity\Training;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TrainingController extends Controller
{

    /**
     * @Route(name="user_cabinet", path="/user/")
     */
    public function indexAction()
    {
        $trainingRepo = $this->getDoctrine()->getRepository('TrainingBundle:Training');
        $trainings = $trainingRepo->findAll();

        return $this->render('user/user.html.twig', ['trainings' => $trainings]);
    }

    /**
     * @Route(name="perform_training", path="/user/training/{id}/")
     */
    public function trainingAction(Training $training)
    {
        $session = $this->get('session');
        $trainingId = $training->getId();
        $questionRepo = $this->getDoctrine()->getRepository('TrainingBundle:Question');
        $answerRepo = $this->getDoctrine()->getRepository('TrainingBundle:Answer');
        $request = Request::createFromGlobals();

        if (!$session->has('training_state_'.$trainingId)) {
            $trainingState = ['number_of_question' => 0, 'score' => 0];
            $session->set('training_state_'.$trainingId, serialize($trainingState));
        } else {
            $trainingState = unserialize($session->get('training_state_' . $trainingId));
            $answerId = $request->request->getInt('answer');
            if ($answerId) {
                $answer = $answerRepo->findOneBy(['id' => $answerId]);
                $trainingState['number_of_question'] += 1;
                $trainingState['score'] += $answer->getScore();
                $session->set('training_state_'.$trainingId, serialize($trainingState));
            }
        }

        $question = $questionRepo->findOneBy([
            'training' => $trainingId,
            'order' => $trainingState['number_of_question']
        ]);

        if (null === $question) {
            return $this->redirectToRoute('training_results', ['id' => $trainingId]);
        }

        $answers = $answerRepo->findBy(['question' => $question->getId()]);

        return $this->render(
            'user/training/perform.html.twig',
            compact('training', 'question', 'answers')
        );
    }

    /**
     * @Route(name="training_results", path="/user/training/{id}/results/")
     */
    public function resultsAction(Training $training)
    {
        $trainingRepo = $this->getDoctrine()->getRepository('TrainingBundle:Training');
        $trainingId = $training->getId();
        $session = $this->get('session');

        $maxScore = $trainingRepo->getMaxTrainingScore($trainingId);
        $trainingState = unserialize($session->get('training_state_'.$trainingId));
        // @TODO: Запись в таблицу результатов, проверку на существование данных в сессии (страницу могут обновить) - в этом случае данные из таблицы
        $session->remove('training_state_'.$trainingId);

        return $this->render('user/training/results.html.twig', compact('training', 'maxScore', 'trainingState'));
    }

}
