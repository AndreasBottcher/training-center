<?php

namespace TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
     private $id;

    /**
     * @var Training
     *
     * @ORM\ManyToOne(targetEntity="TrainingBundle\Entity\Training")
     * @ORM\JoinColumn(nullable=false)
     */
     private $training;

     /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
     private $order;

     /**
      * @var text
      *
      * @ORM\Column(type="text")
      */
     private $questionText;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Question
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set questionText
     *
     * @param string $questionText
     *
     * @return Question
     */
    public function setQuestionText($questionText)
    {
        $this->questionText = $questionText;

        return $this;
    }

    /**
     * Get questionText
     *
     * @return string
     */
    public function getQuestionText()
    {
        return $this->questionText;
    }

    /**
     * Set trainingId
     *
     * @param \TrainingBundle\Entity\Training $trainingId
     *
     * @return Question
     */
    public function setTrainingId(\TrainingBundle\Entity\Training $trainingId)
    {
        $this->trainingId = $trainingId;

        return $this;
    }

    /**
     * Get trainingId
     *
     * @return \TrainingBundle\Entity\Training
     */
    public function getTrainingId()
    {
        return $this->trainingId;
    }
}
