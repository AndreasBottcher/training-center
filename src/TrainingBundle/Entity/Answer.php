<?php

namespace TrainingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
     private $id;

     /**
      * @var Question
      *
      * @ORM\ManyToOne(targetEntity="TrainingBundle\Entity\Question")
      * @ORM\JoinColumn(nullable=false)
      */
      private $question;

      /**
       * @var text
       *
       * @ORM\Column(type="text")
       */
      private $answer;

      /**
       * @var int
       *
       * @ORM\Column(type="integer")
       */
       private $score;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return Answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return Answer
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set questionId
     *
     * @param \TrainingBundle\Entity\Question $questionId
     *
     * @return Answer
     */
    public function setQuestionId(\TrainingBundle\Entity\Question $questionId)
    {
        $this->questionId = $questionId;

        return $this;
    }

    /**
     * Get questionId
     *
     * @return \TrainingBundle\Entity\Question
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }
}
