<?php

namespace TrainingBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TrainingRepository extends EntityRepository
{
    public function getMaxTrainingScore($trainingId)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT MAX(a.score) FROM TrainingBundle:Question q, TrainingBundle:Answer a
                WHERE q.training = :id
                GROUP BY a.question'
            )->setParameter('id', $trainingId);

        try {
            $score = 0;
            foreach ($query->getResult() as $scrArr) {
                $score += $scrArr[1];
            }

            return $score;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
